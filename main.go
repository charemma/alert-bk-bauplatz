package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/gocolly/colly"
)

var (
	URL = "https://www.backnang.de/start/Bauen_+Wohnen+und+Umwelt/Bauplaetze"
)

func alert(msg string) {
	fmt.Printf("Content changed on site\n")
	fmt.Printf("Message: %s\n", msg)
	fmt.Printf(URL)
}

func main() {
	fmt.Println("Scrap website bk bauplaetze")

	c := colly.NewCollector()

	c.OnHTML(".basecontent-line-break-text", func(e *colly.HTMLElement) {
		content := strings.TrimSpace(e.Text)

		if content != "Zur Zeit stehen bei der Stadt Backnang keine Bauplätze zum Verkauf." {
			alert(e.Text)
		}
	})

	c.Visit(URL)
	os.Exit(0)
}
